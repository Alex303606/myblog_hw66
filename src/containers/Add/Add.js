import React, {Component} from 'react'
import './Add.css'
import axios from 'axios'
import withLoadingHandler from "../../hoc/withErrorHandler/withLoadingHandler";

class Add extends Component {
	state = {
		post: {
			title: '',
			text: '',
		}
	};
	
	async componentDidMount() {
		if (this.props.match.params.id) {
			const id = this.props.match.params.id;
			let post = await axios.get(`/posts/${id}.json`);
			this.setState({post: post.data});
		}
	};
	
	changeTextHandler = (event) => {
		const post = {...this.state.post};
		post.text = event.target.value;
		this.setState({post});
	};
	
	changeTitleHandler = (event) => {
		const post = {...this.state.post};
		post.title = event.target.value;
		this.setState({post});
	};
	
	
	savePostHandler = () => {
		let post = {...this.state.post};
		if (this.props.match.params.id) {
			axios.put(`/posts/${this.props.match.params.id}.json`, post).finally(() => {
				this.props.history.replace('/');
			});
		} else {
			post = {
				title: this.state.post.title,
				text: this.state.post.text,
				date: new Date(),
				disabled: true
			};
			axios.post('/posts.json', post).finally(() => {
				this.setState(prevState => {
					return {
						title: prevState.post.title = '',
						text: prevState.post.text = ''
					}
				});
				this.props.history.replace('/posts');
			});
		}
		
	};
	
	render() {
		return (
			<div className="Add">
				<div className="Add__title">Add new post</div>
				<div className="Add__row">
					<label htmlFor="title">Title</label>
					<input onChange={(event) => this.changeTitleHandler(event)} value={this.state.post.title} id="title"
					       type="text"/>
				</div>
				<div className="Add__row">
					<label htmlFor="text">Description</label>
					<textarea onChange={(event) => this.changeTextHandler(event)} value={this.state.post.text}
					          id="text"/>
				</div>
				<button onClick={this.savePostHandler} className="Add__save">SAVE</button>
			</div>
		)
	}
}

export default withLoadingHandler(Add, axios);
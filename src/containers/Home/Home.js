import React, {Component} from 'react'
import axios from "axios/index";
import Post from "../../components/Posts/Post/Post";
import ErrorBoundary from "../../components/UI/ErrorBoundary/ErrorBoundary";
import withLoadingHandler from "../../hoc/withErrorHandler/withLoadingHandler";


class Home extends Component {
	
	state = {
		posts: [],
		loading: false
	};
	
	updatePosts = async () => {
		this.setState({loading: true});
		let posts = await axios.get('/posts.json');
		try {
			let newPosts = [];
			for (let key in posts.data) {
				newPosts.push({
					title: posts.data[key].title,
					id: key,
					date: posts.data[key].date,
					text: posts.data[key].text,
					disabled: posts.data[key].disabled
				});
			}
			this.setState({posts: newPosts,loading: false});
		} catch (error) {
			this.setState({loading: false});
		}
	};
	
	componentDidMount() {
		this.updatePosts();
	}
	
	getFullPost = (id) => {
		this.props.history.push({
			pathname: '/posts/' + id
		})
	};
	
	render() {
		return (
			<ul>
				{
					this.state.posts.map(post => {
						return (
							<ErrorBoundary key={post.id}>
								<Post
									date={post.date}
									title={post.title}
									text={post.text}
									readMore={() => this.getFullPost(post.id)}
								/>
							</ErrorBoundary>
						)
					})
				}
			</ul>
		)
	}
}

export default withLoadingHandler(Home, axios);
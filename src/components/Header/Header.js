import React from 'react'
import {NavLink} from "react-router-dom";
import './Header.css'
const Header = props => {
	return (
		<header>
			<div className="logo">My Blog</div>
			<ul>
				<li><NavLink exact to="/posts">Home</NavLink></li>
				<li><NavLink to="/posts/add">Add</NavLink></li>
				<li><NavLink to="/about">About</NavLink></li>
				<li><NavLink to="/contacts">Contacts</NavLink></li>
			</ul>
		</header>
	)
};

export default Header;
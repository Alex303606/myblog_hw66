import React from 'react'
import Moment from "react-moment";
import './Post.css'
const Post = props => {
	
	if (Math.random() > 0.9) {
		throw new Error('Well, this happened ...');
	}
	
	return (
		<li className="post">
			<div className="post__date">Created on: <Moment format="DD-MM-YY HH:mm" date={props.date}/></div>
			<div className="post__title">{props.title}</div>
			<div className="post__text">{props.text}</div>
			<button onClick={props.readMore} className="post__read-more">Read more</button>
		</li>
	)
};

export default Post;
import React, {Component} from 'react'
import './FullPost.css'
import Moment from "react-moment";
import axios from 'axios'
import Spinner from "../../UI/Spinner/Spinner";
import withLoadingHandler from "../../../hoc/withErrorHandler/withLoadingHandler";

class FullPost extends Component {
	
	state = {
		post: {},
		loading: false
	};
	
	async componentDidMount() {
		this.setState({loading: true});
		const id = this.props.match.params.id;
		let post = await axios.get(`/posts/${id}.json`).finally(() => {
			this.setState({loading: false});
		});
		this.setState({post: post.data});
		post.data.id = id;
	}
	
	deletePostHandler = () => {
		axios.delete(`/posts/${this.state.post.id}.json`).finally(() => {
			this.props.history.replace('/posts');
		});
	};
	
	editPostTextHandler = () => {
		let post = {...this.state.post};
		this.props.history.push({
			pathname: '/posts/' + post.id + '/edit'
		})
	};
	
	render() {
		let info = (
			<div className="FullPost">
				<div className="FullPost__date">Created on: <Moment format="DD-MM-YY HH:mm"
				                                                    date={this.state.post.date}/></div>
				<div className="FullPost__title">Title: {this.state.post.title}</div>
				<div className="FullPost__text">{this.state.post.text}</div>
				<div className="FullPost__buttons">
					<button onClick={this.deletePostHandler} className="FullPost__delete">Delete</button>
					<button onClick={this.editPostTextHandler} className="FullPost__edit">Edit</button>
				</div>
			</div>
		);
		if (this.state.loading) info = <Spinner/>;
		return info;
	}
}

export default withLoadingHandler(FullPost, axios);
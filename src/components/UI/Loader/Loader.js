import React,{Fragment} from 'react'
import Backdrop from "../Backdrop/Backdrop";
import Spinner from "../Spinner/Spinner";

const Loader = props => {
	return (
		<Fragment>
			<Backdrop show={props.loading}/>
			<Spinner show={props.loading}/>
		</Fragment>
	)
};

export default Loader;
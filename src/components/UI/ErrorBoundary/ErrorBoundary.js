import React, {Component} from 'react'
import './ErrorBoundary.css'
import axios from 'axios'
import Spinner from "../Spinner/Spinner";

class ErrorBoundary extends Component {
	state = {
		hasError: false,
		errorMessage: '',
		loading: true
	};
	
	componentDidCatch = (error, info) => {
		axios.post('/errors.json', info).finally(() => this.setState({loading: false}));
		this.setState({hasError: true, errorMessage: error});
	};
	
	render() {
		if (this.state.hasError) {
			return (
				<div className="Error-message">
					<Spinner class='little' show={this.state.loading}/>
					Что то пошло не так! Мы исправим в скором времени.
				</div>
			)
		} else {
			return this.props.children;
		}
	}
}

export default ErrorBoundary;
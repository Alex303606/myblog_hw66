import React from 'react'
import './Spinner.css'
const Spinner = props => (props.show ? <div className={[props.class,'Spinner'].join(' ')}>Loading...</div> : null);

export default Spinner;
import React, {Component} from 'react';
import Header from "./components/Header/Header";
import './App.css'
import {Route, Switch} from "react-router-dom";
import Add from "./containers/Add/Add";
import Home from "./containers/Home/Home";
import About from "./containers/About/About";
import Contact from "./containers/Contacts/Contact";
import FullPost from "./components/Posts/FullPost/FullPost";

class App extends Component {
	render() {
		return (
			<div className="App">
				<Header/>
				<Switch>
					<Route exact path="/" component={Home}/>
					<Route exact path="/posts" component={Home}/>
					<Route exact path="/posts/add" component={Add}/>
					<Route exact path="/posts/:id" component={FullPost}/>
					<Route exact path="/posts/:id/edit" component={Add}/>
					<Route exact path="/about" component={About}/>
					<Route exact path="/contacts" component={Contact}/>
				</Switch>
			</div>
		)
	}
}

export default App;
